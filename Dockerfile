from akashwebdev/upubuntu

run apt-get update 
run apt-get install -y postgis

USER postgres

RUN /etc/init.d/postgresql start

USER root

RUN echo "host all  all    0.0.0.0/0  trust" >> /etc/postgresql/10/main/pg_hba.conf
RUN echo "listen_addresses='*'" >> /etc/postgresql/10/main/postgresql.conf

EXPOSE 5432

RUN mkdir -p /var/run/postgresql 
run chown -R postgres /var/run/postgresql

CMD /etc/init.d/postgresql start
